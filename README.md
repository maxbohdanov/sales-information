# Magento 2: Sales Information
This module will help you collect data on the sold product quantity and its last date of purchase.

```
Qty purchased: <?= $_product->getSalesInformation()->getQty() ?> </br>
Last Order Data: <?= $_product->getSalesInformation()->getLastOrder() ?>
```

## Installation with composer
```bash
cd <magento_root>
composer config repositories.maxbohdanov/sales-information vcs git@bitbucket.org:maxbohdanov/sales-information.git
composer require maxbohdanov/sales-information
php bin/magento module:enable Bohdanov_SalesInfo
php bin/magento setup:upgrade
php bin/magento cache:flush 
```


## Installation without composer 
1. Copy module to `app/code/Bohdanov/SalesInfo` folder.
2. Run 

 * `php bin/magento module:enable Bohdanov_SalesInfo`
 * `php bin/magento setup:upgrade`
 * `php bin/magento cache:flush `
 