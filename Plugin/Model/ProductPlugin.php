<?php
/**
 *  Copyright © 1989-2019 Bohdanov. All rights reserved.
 *  See LICENSE.txt for license details.
 */

namespace Bohdanov\SalesInfo\Plugin\Model;

use Bohdanov\SalesInfo\Model\SalesInformation;
use Magento\Catalog\Api\Data\ProductExtensionFactory;
use Magento\Catalog\Model\Product;

/**
 * Class AfterProductLoad
 * @package Bohdanov\SalesInfo\Model\Plugin
 */
class ProductPlugin
{
    /**
     * @var SalesInformation
     */
    protected $salesInformation;
    /**
     * @var ProductExtensionFactory
     */
    protected $productExtensionFactory;

    /**
     * @param SalesInformation $salesInformation
     * @param ProductExtensionFactory $productExtensionFactory
     */
    public function __construct(
        SalesInformation $salesInformation,
        ProductExtensionFactory $productExtensionFactory
    ) {
        $this->salesInformation = $salesInformation;
        $this->productExtensionFactory = $productExtensionFactory;
    }

    /**
     * Add sales information to the product's extension attributes
     *
     * @param Product $product
     *
     * @return Product
     */
    public function afterLoad(Product $product)
    {
        $productExtension = $product->getExtensionAttributes();
        if ($productExtension === null) {
            $productExtension = $this->productExtensionFactory->create();
        }
        $salesInformationInstance = $this->salesInformation->getSalesInformation($product->getId());
        $productExtension->setSalesInformation($salesInformationInstance);
        $product->setExtensionAttributes($productExtension);
        $product->setSalesInformation($salesInformationInstance);

        return $product;
    }
}
