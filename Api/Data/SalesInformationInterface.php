<?php
/**
 *  Copyright © 1989-2019 Bohdanov. All rights reserved.
 *  See LICENSE.txt for license details.
 */

namespace Bohdanov\SalesInfo\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;
use Bohdanov\SalesInfo\Api\Data\SalesInformationExtensionInterface;

/**
 * Interface SalesInformationInterface
 * @package Bohdanov\SalesInfo\Api\Data
 */
interface SalesInformationInterface extends ExtensibleDataInterface
{
    const CACHE_KEY = 'sales_information';

    const ORDER_STATUS = 'order-status';

    /**
     * @return int
     */
    public function getQty();

    /**
     * @return string
     */
    public function getLastOrder();

    /**
     * @param int $productId
     * @return mixed
     */
    public function getSalesInformation($productId);

    /**
     * Retrieve existing extension attributes object or create a new one.
     *
     * @return SalesInformationExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     *
     * @param SalesInformationExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        SalesInformationExtensionInterface $extensionAttributes
    );
}
