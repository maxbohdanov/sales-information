<?php
/**
 *  Copyright © 1989-2019 Bohdanov. All rights reserved.
 *  See LICENSE.txt for license details.
 */

namespace Bohdanov\SalesInfo\Model;

use Bohdanov\SalesInfo\Api\Data\SalesInformationInterface;
use Bohdanov\SalesInfo\Api\Data\SalesInformationExtensionInterface;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Model\AbstractExtensibleModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\FilterBuilder;
use Magento\Sales\Api\OrderItemRepositoryInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderRepositoryInterface;

/**
 * Class SalesInformation
 * @package Bohdanov\SalesInfo\Model
 */
class SalesInformation extends AbstractExtensibleModel implements SalesInformationInterface
{
    /**
     * @var \Magento\Framework\App\CacheInterface
     */
    protected $cache;

    /**
     * @var string
     */
    protected $productId;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var FilterBuilder
     */
    protected $filterBuilder;

    /**
     * @var SortOrder
     */
    protected $sortOrderBuilder;

    /**
     * @var OrderItemRepositoryInterface
     */
    protected $orderItemRepository;

    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var
     */
    protected $orderedItems;

    /**
     * SalesInformation constructor.
     *
     * @param Context $context
     * @param Registry $registry
     * @param ExtensionAttributesFactory $extensionFactory
     * @param AttributeValueFactory $customAttributeFactory
     * @param OrderItemRepositoryInterface $orderItemRepository
     * @param OrderRepositoryInterface $orderRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param FilterBuilder $filterBuilder
     * @param SortOrder $sortOrderBuilder
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $customAttributeFactory,
        OrderItemRepositoryInterface $orderItemRepository,
        OrderRepositoryInterface $orderRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder,
        SortOrder $sortOrderBuilder,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $resource,
            $resourceCollection,
            $data
        );
        $this->cache = $context->getCacheManager();
        $this->orderRepository = $orderRepository;
        $this->orderItemRepository = $orderItemRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->sortOrderBuilder = $sortOrderBuilder;
    }

    /**
     * @param int $productId
     *
     * @return $this|mixed
     */
    public function getSalesInformation($productId)
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * @return int
     */
    public function getQty()
    {
        $cacheKey = $this->getCacheKey('qty');
        if ($qtyOrdered = $this->cache->load($cacheKey)) {
            return $qtyOrdered;
        }
        $productId = $this->getProductId();
        $orderItems = $this->getOrderItemsByProductId($productId);
        $qtyOrdered = 0;
        foreach ($orderItems as $item) {
            $qtyOrdered += $item->getQtyOrdered();
        }
        $this->cache->save($qtyOrdered, $cacheKey);

        return $qtyOrdered;
    }

    /**
     * @return string
     */
    public function getLastOrder()
    {
        $cacheKey = $this->getCacheKey('last-order');
        if ($lastOrder = $this->cache->load($cacheKey)) {
            return $lastOrder;
        }
        $lastOrder = '';
        $productId = $this->getProductId();
        $orderItems = $this->getOrderItemsByProductId($productId);
        if ($lastOrderedItem = $orderItems->getLastItem()) {
            $lastOrder = (string)$lastOrderedItem->getCreatedAt();
        }
        $this->cache->save($lastOrder, $cacheKey);

        return $lastOrder;
    }

    /**
     * @param $productId
     *
     * @return \Magento\Sales\Api\Data\OrderItemSearchResultInterface
     */
    protected function getOrderItemsByProductId($productId)
    {
        if ($this->orderedItems === null) {
            $orderIds = $this->getFilteredOrderIds();

            $this->searchCriteriaBuilder->addFilters(
                [
                    $this->filterBuilder->setField(OrderItemInterface::PRODUCT_ID)
                        ->setValue($productId)
                        ->setConditionType('eq')
                        ->create(),
                    $this->filterBuilder->setField(OrderItemInterface::ORDER_ID)
                        ->setValue($orderIds)
                        ->setConditionType('in')
                        ->create()
                ]
            );
            $searchCriteria = $this->searchCriteriaBuilder->create();
            $this->orderedItems = $this->orderItemRepository->getList($searchCriteria);
        }

        return $this->orderedItems;
    }

    /**
     * @return mixed
     */
    protected function getFilteredOrderIds()
    {
        if ($orderStatus = $this->getFilterableOrderStatus()) {
            $this->searchCriteriaBuilder->addFilters(
                [
                    $this->filterBuilder->setField(OrderInterface::STATUS)
                        ->setValue($orderStatus)
                        ->setConditionType('eq')
                        ->create()
                ]
            );
        }
        $searchCriteria = $this->searchCriteriaBuilder->create();
        $this->orderedItems = $this->orderRepository->getList($searchCriteria);

        return $this->orderedItems->getAllIds();
    }

    /**
     * @return string
     */
    protected function getFilterableOrderStatus()
    {
        $orderStatus = '';
        if (array_key_exists(self::ORDER_STATUS, $this->_data)) {
            $orderStatus = $this->_data[self::ORDER_STATUS];
        }

        return $orderStatus;
    }

    /**
     * @return string
     */
    protected function getProductId ()
    {
        return $this->productId;
    }

    /**
     * @param $type
     *
     * @return string
     */
    protected function getCacheKey($type)
    {
        $orderStatus = $this->getFilterableOrderStatus();
        $productId = $this->getProductId();
        return self::CACHE_KEY . '_' . $type . '_' . $productId . '_' . $orderStatus;
    }

    /**
     * {@inheritdoc}
     *
     * @return SalesInformationExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * {@inheritdoc}
     *
     * @param SalesInformationExtensionInterface $extensionAttributes
     *
     * @return $this
     */
    public function setExtensionAttributes(
        SalesInformationExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }
}
