<?php
/**
 * Copyright © 1989-2019 Bohdanov. All rights reserved.
 * See LICENSE.txt for license details.
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Bohdanov_SalesInfo',
    __DIR__
);
